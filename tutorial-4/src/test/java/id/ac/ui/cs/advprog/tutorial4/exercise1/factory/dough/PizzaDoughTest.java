package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PizzaDoughTest {
    private Dough pizzaDough;

    @Before
    public void setUp() {
        pizzaDough = new PizzaDough();
    }

    @Test
    public void testToString() {
        assertEquals("Pizza Dough", pizzaDough.toString());
    }
}

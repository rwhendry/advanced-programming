package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class AbcSauceTest {
    private Sauce abcSauce;

    @Before
    public void setUp() {
        abcSauce = new AbcSauce();
    }

    @Test
    public void testToString() {
        assertEquals("Saus Abc terenak", abcSauce.toString());
    }
}

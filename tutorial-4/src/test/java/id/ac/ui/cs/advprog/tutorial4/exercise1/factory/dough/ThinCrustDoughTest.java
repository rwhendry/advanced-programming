package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThinCrustDoughTest {

    private Dough thinCrustDough;

    @Before
    public void setUp() {
        thinCrustDough = new ThinCrustDough();
    }

    @Test
    public void testToString() {
        assertEquals("Thin Crust Dough", thinCrustDough.toString());
    }
}

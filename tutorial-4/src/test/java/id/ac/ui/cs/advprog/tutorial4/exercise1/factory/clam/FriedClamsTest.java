package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FriedClamsTest {

    private Clams friedClams;

    @Before
    public void setUp() {
        friedClams = new FriedClams();
    }

    @Test
    public void testToString() {
        assertEquals("Fried Clams", friedClams.toString());
    }
}

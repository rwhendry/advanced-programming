package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;

import org.junit.Before;
import org.junit.Test;


public class CheesePizzaTest {
    PizzaIngredientFactory newYorkIngredientFactory, depokIngredientFactory;
    Pizza depokCheesePizza, newYorkCheesePizza;

    @Before
    public void setUp() {
        newYorkIngredientFactory = new NewYorkPizzaIngredientFactory();
        depokIngredientFactory = new DepokPizzaIngredientFactory();
        depokCheesePizza = new CheesePizza(depokIngredientFactory);
        newYorkCheesePizza = new CheesePizza(newYorkIngredientFactory);
    }

    @Test
    public void testPrepareDepokCheesePizza() {
        newYorkCheesePizza.prepare();
        assertTrue(newYorkCheesePizza.dough.getClass().isAssignableFrom(ThinCrustDough.class));
    }

}

package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import org.junit.Before;
import org.junit.Test;


public class NewYorkPizzaStoreTest {
    PizzaStore ny;

    @Before
    public void setUp() {
        ny = new NewYorkPizzaStore();
    }

    @Test
    public void testCreatePizza() {
        Pizza pizza = ny.createPizza("");
        assertNull(pizza);
    }

    @Test
    public void testCreateCheesePizza() {
        Pizza pizza = ny.createPizza("cheese");
        assertEquals(pizza.getName(), "New York Style Cheese Pizza");
    }

    @Test
    public void testCreateVeggiePizza() {
        Pizza pizza = ny.createPizza("veggie");
        assertEquals(pizza.getName(), "New York Style Veggie Pizza");
    }

    public void testCreateClamPizza() {
        Pizza pizza = ny.createPizza("clam");
        assertEquals(pizza.getName(), "New York Style Clam Pizza");

    }
}

package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
    PizzaStore depokPizzaStore;

    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void testCreatePizza() {
        Pizza pizza = depokPizzaStore.createPizza("");
        assertNull(pizza);
    }

    @Test
    public void testCreateCheesePizza() {
        Pizza pizza = depokPizzaStore.createPizza("cheese");
        assertEquals(pizza.getName(), "Depok Cool Style Cheese Pizza");
    }

    @Test
    public void testCreateVeggiePizza() {
        Pizza pizza = depokPizzaStore.createPizza("veggie");
        assertEquals(pizza.getName(), "Depok Cool Style Veggie Pizza");
    }

    public void testCreateClamPizza() {
        Pizza pizza = depokPizzaStore.createPizza("clam");
        assertEquals(pizza.getName(), "Depok Cool Style Clam Pizza");

    }
}

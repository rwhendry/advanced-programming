package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class PizzaDough implements Dough {
    public String toString() {
        return "Pizza Dough";
    }
}

package hello;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = Cv.class)
public class CvTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void openCvWithoutUser() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("This is my CV")));
    }

    @Test
    public void openCvWithtUser() throws Exception {
        mockMvc.perform(get("/cv?visitor=rey"))
                .andExpect(content().string(containsString("rey , I hope you are interested "
                        + "to hire me")));
    }
}

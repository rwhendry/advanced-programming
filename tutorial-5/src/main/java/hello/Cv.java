package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class Cv {
    @GetMapping("/cv")
    public String greeting(@RequestParam(name = "visitor", required = false)
                                   String name, Model model) {
        if (name != null) {
            model.addAttribute("name", name);
        }
        return "cv";
    }
}

package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class BackendProgrammer extends Employees {
    public static final double BASE_SALARY = 20000.00;

    public BackendProgrammer(String name, double salary) {
        if (salary < BASE_SALARY) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.salary = salary;
        role = "Back End Programmer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}

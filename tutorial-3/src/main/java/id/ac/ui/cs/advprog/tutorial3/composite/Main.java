package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Company company = new Company();

        ArrayList<Employees> listEmployees = new ArrayList<Employees>();
        listEmployees.add(new Ceo("Rey", 5000000.00));
        listEmployees.add(new Cto("Cahya", 2000000.00));
        listEmployees.add(new BackendProgrammer("Distra", 100000.00));
        listEmployees.add(new BackendProgrammer("Kusuman", 50000.00));
        listEmployees.add(new FrontendProgrammer("Raihan", 300000.00));
        listEmployees.add(new NetworkExpert("FwP", 1000000.00));
        listEmployees.add(new SecurityExpert("Febri", 200000.00));
        listEmployees.add(new UiUxDesigner("Firman", 900000.00));

        for (Employees employees: listEmployees) {
            company.addEmployee(employees);
            System.out.println("Company hire employee with role"
                    +  employees.getRole() + " " + "name: " + employees.getName()
                    + " and salary: " + employees.getSalary());
        }

    }
}

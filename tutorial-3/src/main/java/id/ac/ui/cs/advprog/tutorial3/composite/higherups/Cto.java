package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public static final double BASE_SALARY = 100000.00;

    public Cto(String name, double salary) {
        if (salary < BASE_SALARY) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.salary = salary;
        role = "CTO";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}

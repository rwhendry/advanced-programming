package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class Main {
    public static void main(String[] args) {
        for (BreadProducer bread: BreadProducer.values()) {
            Food food = bread.createBreadToBeFilled();
            System.out.println("Decorating " + food.getDescription());

            for (FillingDecorator filling: FillingDecorator.values()) {
                food = filling.addFillingToBread(food);
            }

            System.out.println(food.getDescription() + "ready to be served with cost: "
                    + food.cost());
        }
    }
}

package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {
    public static final double BASE_SALARY = 90000.00;

    public UiUxDesigner(String name, double salary) {
        if (salary < BASE_SALARY) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.salary = salary;
        role = "UI/UX Designer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}

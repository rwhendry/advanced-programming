package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public static final double BASE_SALARY = 200000.00;

    public Ceo(String name, double salary) {
        if (salary < BASE_SALARY) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.salary = salary;
        role = "CEO";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
